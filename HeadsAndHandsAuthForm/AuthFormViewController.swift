//
//  AuthFormViewController.swift
//  HeadsAndHandsAuthForm
//
//  Created by Maxim Vekovenko on 17.08.2019.
//  Copyright © 2019 Heads ans Hands. All rights reserved.
//

import UIKit
import RxSwift

class AuthFormViewController: UIViewController {
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var topConstant: NSLayoutConstraint!
    @IBOutlet weak var registerButton: UIButton!

    @IBOutlet weak var authButton: UIButton!
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setUpView()
        mailTextField.delegate = self
        passwordTextField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        // Test
        //mailTextField.text = "mail@mail.ru"
        //passwordTextField.text = "12aAsb"
    }
    
    
    @IBAction func authButtonTapped(_ sender: Any) {
        let passwordString = self.passwordTextField.text?.matchingPassword()
        let loginString = self.mailTextField.text?.isValidEmail()
        var messageString = ""
        if (loginString!)
        {
            print("login ok")
            if passwordString!.count > 0
            {
                print("pass ok")
            }else
            {
                print("pass not ok")
                messageString = "Проверьте правильность ввода пароля"
            }
            
        }else
        {
            print("login not ok")
            messageString = "Проверьте правильность ввода email"
            
        }
        if messageString.count > 0
        {
            self.presentAlertWith(message: messageString, title: "Ошибка")
        }else
        {
            ApiClient.getWeather()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { weatherList in
                    print("List of weather:", weatherList)
                    self.presentAlertWith(message:"Текущая температура в Лондоне: "+String(weatherList.main.temp),title: "Информация")
                }, onError: { error in
                    switch error {
                    case ApiError.conflict:
                        print("Conflict error")
                    case ApiError.forbidden:
                        print("Forbidden error")
                    case ApiError.notFound:
                        print("Not found error")
                    default:
                        print("Unknown error:", error)
                    }
                })
                .disposed(by: disposeBag)
        }
        
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
        scrollView.scrollRectToVisible(registerButton.frame, animated: true)
        
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func setUpView()
    {
        authButton.setTitle("Войти", for: .normal)
        authButton.setTitleColor(UIColor.white, for: .normal)
        authButton.backgroundColor = TANGERINE_COLOR
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    
    }
    
}

extension AuthFormViewController: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
}
