//
//  ApiError.swift
//  HeadsAndHandsAuthForm
//
//  Created by Maxim Vekovenko on 21.08.2019.
//  Copyright © 2019 Heads ans Hands. All rights reserved.
//

import UIKit

enum ApiError: Error {
    case forbidden              //Status code 403
    case notFound               //Status code 404
    case conflict               //Status code 409
    case internalServerError    //Status code 500
}
