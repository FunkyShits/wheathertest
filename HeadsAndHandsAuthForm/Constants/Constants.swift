//
//  Constants.swift
//  HeadsAndHandsAuthForm
//
//  Created by Maxim Vekovenko on 17.08.2019.
//  Copyright © 2019 Heads ans Hands. All rights reserved.
//

import Foundation
import UIKit

let TANGERINE_COLOR = UIColor(hex: 0xff9b00)

struct Constants {
    
    //The API's base URL
    static let baseUrl = "http://api.openweathermap.org/data/2.5/weather?id=2643743&units=metric&APPID=c2afea77f9af030fc588ba6095f91a0e"
    

    //The header fields
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
    //The content type (JSON)
    enum ContentType: String {
        case json = "application/json"
    }
}


