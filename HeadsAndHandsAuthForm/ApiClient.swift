//
//  ApiClient.swift
//  HeadsAndHandsAuthForm
//
//  Created by Maxim Vekovenko on 21.08.2019.
//  Copyright © 2019 Heads ans Hands. All rights reserved.
//

import Alamofire
import RxSwift

class ApiClient {
    
    static func getWeather() -> Observable<WeatherStruct> {
        return request(ApiRouter.getWeather())
    }
    
    //-------------------------------------------------------------------------------------------------
    //MARK: - The request function to get results in an Observable
    private static func request<T: Codable> (_ urlConvertible: URLRequestConvertible) -> Observable<T> {
        //Create an RxSwift observable, which will be the one to call the request when subscribed to
        return Observable<T>.create { observer in
           let request = AF.request(urlConvertible).responseDecodable { (response: DataResponse<T>) in
                print(T.self)
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    //Everything is fine, return the value in onNext
                    observer.onNext(value)
                    observer.onCompleted()
                case .failure(let error):
                    //Something went wrong, switch on the status code and return the error
                    switch response.response?.statusCode {
                    case 403:
                        observer.onError(ApiError.forbidden)
                    case 404:
                        observer.onError(ApiError.notFound)
                    case 409:
                        observer.onError(ApiError.conflict)
                    case 500:
                        observer.onError(ApiError.internalServerError)
                    default:
                        observer.onError(error)
                    }
                }
            }
            
            return Disposables.create {
                request.cancel()
            }
            
            
        
        }
    
}
}
