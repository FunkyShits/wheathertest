//
//  ApiRouter.swift
//  HeadsAndHandsAuthForm
//
//  Created by Maxim Vekovenko on 21.08.2019.
//  Copyright © 2019 Heads ans Hands. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

enum ApiRouter: URLRequestConvertible {
    
    case getWeather()
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.baseUrl.asURL()
        
        var urlRequest = URLRequest(url: url)
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return JSONEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: nil)
    }
    
    //MARK: - HttpMethod
    private var method: HTTPMethod {
        switch self {
        case .getWeather:
            return .get
        }
    }
    
}
